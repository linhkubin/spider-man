﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spider : MonoBehaviour
{

    public Rigidbody2D rb;
    public GameObject g_hook;

    private Rigidbody2D hook;
    private Rigidbody2D GetHook()
    {
        if (hook == null)
        {
            hook = g_hook.GetComponent<Rigidbody2D>();

            GetComponent<SpringJoint2D>().connectedBody = hook;
        }
        return hook;
    }

    private SpringJoint2D spring;
    private SpringJoint2D GetSpring()
    {
        if (spring == null)
        {
            spring = GetComponent<SpringJoint2D>();
        }
        return spring;
    }

    public float releaseTime = .15f;
    public float maxDragDistance = 2f;

    public GameObject nextBall;

    private bool isPressed = false;

    void Update()
    {
        if (isPressed)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Vector3.Distance(mousePos, GetHook().position) > maxDragDistance)
                rb.position = GetHook().position + (mousePos - GetHook().position).normalized * maxDragDistance;
            else
                rb.position = mousePos;
        }
    }

    void OnMouseDown()
    {
        Debug.Log("down");
        isPressed = true;
        rb.isKinematic = true;
    }

    void OnMouseUp()
    {
        isPressed = false;
        rb.isKinematic = false;

        StartCoroutine(Release());
    }

    IEnumerator Release()
    {
        yield return new WaitForSeconds(releaseTime);

        GetSpring().enabled = false;
        //this.enabled = false;

        //yield return new WaitForSeconds(2f);

        //if (nextBall != null)
        //{
        //    nextBall.SetActive(true);
        //}
        //else
        //{
        //    Enemy.EnemiesAlive = 0;
        //    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //}

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Obstacle")
        {
            if (collision.gameObject != g_hook)
            {
                Debug.Log("impact");
                g_hook = collision.gameObject;
                hook = null;
                GetHook();
                GetSpring().enabled = true;
            }
        }
    }

}
